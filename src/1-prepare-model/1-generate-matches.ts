// import * as _ from 'lodash';
// import * as fs from 'fs-extra';
// import { Piece } from '../helpers/piece';
// import { Board } from '../helpers/board';
// import { MoveInfo } from '../helpers/entity';
// import * as promiseUtils from 'blend-promise-utils';
// import { playRandomMatches } from '../helpers/play-random-matches';
// import { IPlayMatchOptions } from '../helpers/solver';
//
// const uuidv1 = require('uuid/v1');
//
// const stats = require('stats-lite');
//
// const options: IPlayMatchOptions = {
// 	generation: 1, // '1-trained-on-1000';
// 	numberOfMatches: 1,
// 	batchSize: 1,
// 	modelName: `data-1000000-gen-0`,
// };
//
// async function generateRandomMatches(options: IPlayMatchOptions) {
// 	try {
// 		console.log(`Playing ${options.numberOfMatches} matches`);
//
// 		let lastPercentageReported = -1;
//
// 		// Generate moves
// 		for (let batchIndex = 0; batchIndex < options.numberOfMatches / options.batchSize; batchIndex++) {
// 			const moves: MoveInfo[][] = await playRandomMatches(options.batchSize, options);
//
// 			let csvOutput = _.times(100, (columnIndex: number) => 'column_' + columnIndex).join(CSV_DELIMITER) + ',score\n';
// 			csvOutput += _.map(_.flatten(moves), (move) => {
// 				return move.board.toCsv() + CSV_DELIMITER + move.order; // calculateBoardScoreDummy(move, statistics.max)
// 			}).join('\n');
// 			let uuid = uuidv1();
// 			await fs.writeFile(`data/data-${options.numberOfMatches}-gen-${options.generation}-${uuid}.json`, JSON.stringify(moves));
// 			await fs.writeFile(`data/data-${options.numberOfMatches}-gen-${options.generation}-${uuid}.csv`, csvOutput);
//
// 			const percentage = Math.round(batchIndex * 1000 * 100 / options.numberOfMatches);
// 			if (percentage !== lastPercentageReported) {
// 				lastPercentageReported = percentage;
// 				console.log(percentage + ' %');
// 			}
// 		}
//
// 		// const matchLengths: number[] = [];
// 		// let matchLength = 0;
// 		// _.forEach(moves, (move) => {
// 		// 	matchLength += 1;
// 		// 	if (move.order === 0 && matchLength) {
// 		// 		matchLengths.push(matchLength);
// 		// 		matchLength = 0;
// 		// 	}
// 		// });
//
// 		// output statistics
// 		// let statistics = {
// 		// 	min: _.min(matchLengths),
// 		// 	mean: stats.mean(matchLengths),
// 		// 	median: stats.median(matchLengths),
// 		// 	max: _.max(matchLengths),
// 		// 	variance: stats.variance(matchLengths),
// 		// 	standardDeviation: stats.stdev(matchLengths),
// 		// };
// 		// await fs.writeFile(`data/data-${options.numberOfMatches}-gen-${GENERATION}-statistics.json`, JSON.stringify(statistics, null, 2));
//
// 		// Output to file
//
// 		console.log('done');
// 	} catch (err) {
// 		console.error('Error: ', err);
// 	}
// }
//
// generateRandomMatches(options);
//
//
//
