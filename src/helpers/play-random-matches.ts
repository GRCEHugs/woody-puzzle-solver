import { MoveInfo, OneTwoThree, ThreePieceMoveInfo } from './entity';
import { Board } from './board';
import { Piece } from './piece';
import * as _ from 'lodash';
import {
	findPieceIndex,
	findPieceIndexes,
	getBestPieceScoreBySinglePiece, getRandomMove,
	getRandomPiece,
	IPlayMatchOptions, printPieces,
} from './solver';
import * as promiseUtils from 'blend-promise-utils';
import { move } from 'fs-extra';

export function playRandomMatch(): ThreePieceMoveInfo[] {
	let endGame = false;
	let consecutiveThreePieceMoves: ThreePieceMoveInfo[] = [];
	let board = new Board();
	let movesPlayedSoFar = 0;
	while (!endGame) {
		let consecutiveMoves: MoveInfo[] = [];
		let playablePieces: Piece[] = _.times(3, getRandomPiece);
		let playableIndexes: OneTwoThree[] = [0, 1, 2];
		while (playablePieces.length && !endGame) {
			let moveInfo: MoveInfo | null = getRandomMove(board, playablePieces, playableIndexes);
			// try {
			// 	findPieceIndex(playablePieces, moveInfo.piece);
			// } catch (err) {
			// 	console.log('playable pieces');
			// 	printPieces(playablePieces);
			// }
			if (moveInfo) {
				// console.log('played piece:');
				// printPieces([moveInfo.piece]);
				// Remove the piece from the playable pieces
				// console.log('trying to find piece:');
				// console.log(moveInfo.piece.toString());
				// console.log('in array:');
				// printPieces(playablePieces);
				// let pieceIndex: OneTwoThree = findPieceIndex(playablePieces, moveInfo.piece) as OneTwoThree;
				// if (_.isUndefined(pieceIndex)) {
				// 	console.log('cannot find index for playable piece', playablePieces, moveInfo.piece);
				// }
				// console.log('played piece index: ', moveInfo.pieceIndex);
				playablePieces.splice(_.indexOf(playableIndexes, moveInfo.pieceIndex), 1);
				playableIndexes.splice(_.indexOf(playableIndexes, moveInfo.pieceIndex), 1);
				movesPlayedSoFar += 1;
				// console.log('movesPlayedSoFar: ' + movesPlayedSoFar);
				consecutiveMoves.push(moveInfo);
				board = moveInfo.board;
			} else {
				// end game => no more moves
				endGame = true;
			}
		}

		if (consecutiveMoves.length !== 3) {
			continue;
		}

		const firstMove = consecutiveMoves[consecutiveMoves.length - 3];
		const secondMove = consecutiveMoves[consecutiveMoves.length - 2];
		const thirdMove = consecutiveMoves[consecutiveMoves.length - 1];
		// const pieceIndexes = findPieceIndexes(originalPlayablePieces, [_.get(firstMove, 'piece'), _.get(secondMove, 'piece'), _.get(thirdMove, 'piece')]);

		//
		// if (
		// 	!_.includes([0, 1, 2], pieceIndexes[0]) ||
		// 	!_.includes([0, 1, 2], pieceIndexes[1]) ||
		// 	!_.includes([0, 1, 2], pieceIndexes[2])
		// ) {
		// 	console.log('original pieces');
		// 	printPieces(originalPlayablePieces);
		// 	printPieces([_.get(firstMove, 'piece'), _.get(secondMove, 'piece'), _.get(thirdMove, 'piece')]);
		// 	console.log(pieceIndexes);
		// }

		// Only output moves that were able to place all 3 pieces to avoid lots of edge cases in the code
		if (firstMove && secondMove && thirdMove) {
			consecutiveThreePieceMoves.push({
				firstMove,
				secondMove,
				thirdMove,
			});
		}
	}

	_.forEach(consecutiveThreePieceMoves, (move: ThreePieceMoveInfo, index: number) => move.firstMove.order = consecutiveThreePieceMoves.length - 1 - index);
	return consecutiveThreePieceMoves;
}

// export async function playRandomMatches(amount: number, options: IPlayMatchOptions): ThreePieceMoveInfo[][] {
// 	return await promiseUtils.mapLimit(_.times(amount), 20, () => playRandomMatch());
// }
